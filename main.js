const theme = document.createElement("link");
theme.setAttribute("rel", "stylesheet");
theme.setAttribute("href", "./styles/theme.css");

document.addEventListener("DOMContentLoaded", init)

function init() {
if (localStorage.getItem("key") !== null){
    document.querySelector("head").appendChild(theme);
} else {
    document.querySelector("head").removeChild(theme);
}
}

const btn = document.querySelector(".change-theme-btn")

btn.addEventListener("click" , function(){
if (!document.querySelector(`[href="./styles/theme.css"]`)){
    document.querySelector("head").appendChild(theme); 
    localStorage.setItem("key", "theme")
   
} else {
    document.querySelector("head").removeChild(theme);
    localStorage.removeItem("key")
}
})
